<@requirement.CONSTRAINT 'my-demo' 'true' />
<@requirement.PARAM name='JAVA_OPTS' value='-Xmx1g -Djava.awt.headless=true' scope='global' />
<@requirement.PARAM name='JMX_PORT' value='' required='false' type='port' />
<@requirement.PARAM name='TAG' type='tag' />

<@swarm.SERVICE 'my-demo-${namespace}' 'registry.gitlab.com/anthon.arefyev/mytest:${PARAMS.TAG}'>
<@service.SCALABLE />
<@service.CONSTRAINT 'my-demo' 'true' />
<@service.NETWORK 'net-${namespace}' />
<@service.PORT '8080' '8080' />
<@service.PORT PARAMS.JMX_PORT '9876' 'host' />
<@service.ENV 'HOST' 'clickhouse-${namespace}' />
<@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
<@service.ENV 'JMX_PORT' PARAMS.JMX_PORT />
</@swarm.SERVICE>
