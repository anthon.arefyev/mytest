#!/bin/bash

source ./testlib.sh

set -e

ARTIFACT=mytest

out "Create success tag: ${CI_COMMIT_REF_NAME}__${ARTIFACT}-${COMMIT_DATE}-${COMMIT_SHORT}"
git remote set-url origin "https://gitlab-ci-token:${SECRET_TOKEN}@gitlab.com/anthon.arefyev/${ARTIFACT}.git"
git tag ${CI_COMMIT_REF_NAME}__${ARTIFACT}-${COMMIT_DATE}-${COMMIT_SHORT} && git push --tags