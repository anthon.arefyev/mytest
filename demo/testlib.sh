#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
COLOR_OFF='\033[0m'
MAGENTA='\033[0;35m'

function out {
  echo -e "${MAGENTA}[IMAGENARIUM]: $1${COLOR_OFF}"
}

function warningOut {
  echo -e "${YELLOW}[WARNING]: $1${COLOR_OFF}"
}

function showStatus {
  status=$1

  echo -e "\n"

  if [[ "${status}" == 0 ]];then
    echo -e "${GREEN}***********************"
    echo -e "${GREEN}[IMAGENARIUM]: Success"
    echo -e "${GREEN}***********************"    
  else
    echo -e "${RED}xxxxxxxxxxxxxxxxxxxxxx"
    echo -e "${RED}[IMAGENARIUM]: Failed"
    echo -e "${RED}xxxxxxxxxxxxxxxxxxxxxx"
  fi

  echo -e "${COLOR_OFF}"
}
