package org.my.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/demo")
public class DemoController {

    @GetMapping(path = "/1")
    public String getDemo1() {
        return "test demo app";
    }

    @GetMapping(path = "/2")
    public String getDemo2() {
        return "test demo app 2";
    }
}
