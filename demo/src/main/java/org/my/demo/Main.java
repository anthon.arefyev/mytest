package org.my.demo;

import org.protobeans.mvc.MvcEntryPoint;
import org.protobeans.mvc.annotation.EnableMvc;
import org.protobeans.clickhouse.annotation.EnableClickHouse;
import org.protobeans.undertow.annotation.EnableUndertow;
import org.springframework.context.annotation.ComponentScan;

import java.util.UUID;

@EnableUndertow
@EnableMvc(resourcesPath = "static", resourcesUrl = "static")
//@EnableClickHouse(dbHost = "e:clickHouseDbIp", dbPort = "e:clickHouseDbPort", user = "default", password = "")
@ComponentScan(basePackages = "org.my.demo")
public class Main {

    public static void main(String[] args) {
        MvcEntryPoint.run(Main.class);
//        System.out.println(UUID.randomUUID());
    }
}
